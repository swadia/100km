import React from "react";
import MapView from "react-native-maps";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Alert,
  Linking,
} from "react-native";
import * as Location from "expo-location";
import { AdMobBanner, AdMobInterstitial } from "expo-ads-admob";
import { mapStyle } from "./src/shared/mapStyle";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      permissionToUseLocation: false,
      positionIsLoaded: false,
      position: null,
      status: "IDLE",
      markers: [],
    };

    this.requestLocationPermission = this.requestLocationPermission.bind(this);
    this.startAddingLocation = this.startAddingLocation.bind(this);
    this.addMarker = this.addMarker.bind(this);
    this.startRemovingLocation = this.startRemovingLocation.bind(this);
    this.removeMarker = this.removeMarker.bind(this);
    this.showMarkers = this.showMarkers.bind(this);
    this.getCertif = this.getCertif.bind(this);
  }

  componentDidMount() {
    this.requestLocationPermission();
  }
    async showInterstitial() {
        await AdMobInterstitial.setAdUnitID('ca-app-pub-7434465913635228/8331469018');
        await AdMobInterstitial.requestAdAsync({ servePersonalizedAds: true});
        await AdMobInterstitial.showAdAsync();
    }

  async requestLocationPermission() {
    try {
      await Location.requestPermissionsAsync().then((res) => {
        if (res.status === "granted") {
          this.setState({ permissionToUseLocation: true });
          navigator.geolocation.watchPosition(
            (position) => {
              const newPositon = position.coords;
              this.setState({ position: newPositon, positionIsLoaded: true });
            },
            (error) => alert(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
          );
        }
      });
    } catch (err) {
      console.warn(err);
    }
  }

  async startAddingLocation() {
    Alert.alert("appuyer sur la carte pour ajouter une adresse");
    this.setState({ status: "ADDING" });
    this.showInterstitial();
  }

  addMarker(mapEvent) {
    if (this.state.status === "ADDING") {
      let currentMarkers = this.state.markers;
      currentMarkers.push({
        latitude: mapEvent.nativeEvent.coordinate.latitude,
        longitude: mapEvent.nativeEvent.coordinate.longitude,
        id: mapEvent.timeStamp,
      });
      this.setState({ markers: currentMarkers });
      this.setState({ status: "IDLE" });
    }
  }

  startRemovingLocation() {
    Alert.alert("appuyer sur un markeur pour le supprimer");
    this.setState({ status: "REMOVING" });
  }

  removeMarker(Markerevent) {
    if (this.state.status === "REMOVING") {
      let currentMarkers = this.state.markers;
      currentMarkers.forEach(function (item, index, object) {
        if (
          item.latitude === Markerevent.nativeEvent.coordinate.latitude &&
          item.longitude === Markerevent.nativeEvent.coordinate.longitude
        ) {
          object.splice(index, 1);
        }
      });
      this.setState({ markers: currentMarkers });
      this.setState({ status: "IDLE" });
    }
  }

  showMarkers() {
    return this.state.markers.map((marker) => {
      return (
        <View key={marker.id}>
          <MapView.Marker
            coordinate={{
              latitude: marker.latitude,
              longitude: marker.longitude,
            }}
            onPress={(event) => {
              this.removeMarker(event);
            }}
          />
          <MapView.Circle
            center={{
              latitude: marker.latitude,
              longitude: marker.longitude,
            }}
            radius={100000}
            fillColor={"rgba(40, 190, 35, 0.47)"}
          />
        </View>
      );
    });
  }

  getCertif() {
    Linking.openURL("https://media.interieur.gouv.fr/deplacement-covid-19");
  }

  render() {
    if (this.state.permissionToUseLocation) {
      return (
        <View style={styles.container}>
          <MapView
            style={styles.mapStyle}
            onPress={() => {
              console.log(this.state.position);
            }}
            initialRegion={{
              latitude: 46.2276,
              longitude: 2.2137,
              latitudeDelta: 15,
              longitudeDelta: 15,
            }}
            onPress={(event) => this.addMarker(event)}
            customMapStyle={mapStyle}
          >
            {this.state.position ? (
              <MapView.Marker coordinate={this.state.position}>
                <MapView.Callout>
                  <Text> Vous </Text>
                </MapView.Callout>
              </MapView.Marker>
            ) : null}

            {this.showMarkers()}
          </MapView>

          {/* Button to start adding an adresse */}
          <TouchableOpacity
            style={{
              position: "absolute",
              top: "6%",
              left: "6%",
              backgroundColor: "white",
              borderRadius: 37,
              borderWidth: 2,
              width: 60,
              height: 60,
              justifyContent: "center",
              alignItems: "center",
            }}
            elevation={8}
            onPress={this.startAddingLocation}
          >
            <Text style={{ fontSize: 40 }}> + </Text>
          </TouchableOpacity>

          {/* Button to start deleting an adresse */}
          <TouchableOpacity
            style={{
              position: "absolute",
              top: "6%",
              left: "6%",
              backgroundColor: "white",
              borderRadius: 37,
              borderWidth: 2,
              width: 60,
              height: 60,
              marginTop: 70,
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={this.startRemovingLocation}
          >
            <Text style={{ fontSize: 40 }}> - </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              position: "absolute",
              bottom: 60,
              backgroundColor: "white",
              borderRadius: 5,
              borderWidth: 2,
              width: 300,
              height: "8%",
              justifyContent: "center",
              alignItems: "center",
            }}
            onPress={this.getCertif}
          >
            <Text style={{ fontSize: 30 }}> Génerer attestation </Text>
          </TouchableOpacity>
          <View
            style={{
              position: "absolute",
              bottom: 5,
              width: 320,
              height: 50,
            }}
          >
            <AdMobBanner
              bannerSize="banner"
              adUnitID="ca-app-pub-7434465913635228/8561611292"
            />
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <Text> veuillez activer les services de localisation </Text>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  mapStyle: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
});
